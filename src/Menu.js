import React from 'react'
import './css/menu.css'
import add_btn from './img/add_btn.png'
import btn from './img/layers.svg'
import SingUpForm from './SingUpForm'

class Menu extends React.Component{
    singUpClick(){
        alert("Clicked SingUp")
    }
    render(){
    return(
        <div class="title-box">
            <button class="add_card">
                Add new card
            </button>
            <h1 class="title">todo board</h1>
            <button class="login" onClick={this.singUpClick.bind(this)}>sing up</button>
        </div>
    );
}
}
export default Menu