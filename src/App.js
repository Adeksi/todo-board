import React from 'react';
//import logo from './logo.svg';
import './css/App.css';
import './css/reset200802.css'
import Menu from './Menu'
import Board from './Board'


function App() {
  return (
      <div className="App">
        <Menu />
        <Board />
      </div>
  )
}

export default App;
