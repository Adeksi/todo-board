import React from 'react';
import './css/Card.css';
import edit_btn from './img/edit.svg';
import more_btn from './img/more.svg';
import trash_btn from './img/trash.svg'



class Card extends React.Component{
    constructor(props){
        super(props);
        this.state={
            disp: [
                "none",
                "block"
            ],
            flag: 0

        }
    };
    moreClick(){
        let i = 0
        if(this.state.flag === 0){
            i=1
        }
        else{
            i=0
        }
        this.setState({
            flag:i
        }) 
    }
    deleteClick(){
        alert("Deleted")
    }
    editClick(){
        alert("Edited")
    }
    render(){
        let disp = this.state.disp;
    return(
            <div class="content">
                {/* <img src={edit_btn} alt="Edit" className="edit_btn"/> */}
                <img src={more_btn} alt="More" className="more_btn" onClick={this.moreClick.bind(this)}/>
                <div className="more_items" style={{display:disp[this.state.flag]}}>
                    <ul>
                        <li><button className="btn" onClick={this.deleteClick.bind(this)}><img src={trash_btn} alt="Delete" className="trash_btn"/>Delete</button></li>
                        <li><button className="btn" onClick={this.editClick.bind(this)}><img src={edit_btn} alt="Edit" className="edit_btn"/>Edit</button></li>
                    </ul>
                </div>
                <h1 className="card_title">Homework</h1>
                <div className="card_body">
                Dolor amet nostrud est reprehenderit reprehenderit nulla amet reprehenderit nulla. Veniam sint irure laboris officia amet veniam laboris nulla ex proident occaecat anim consequat. Mollit proident sit et qui eu. Adipisicing id aliquip deserunt nulla sit. Elit deserunt quis quis quis sunt ad dolore et. Veniam mollit ex et anim ullamco eiusmod irure quis nulla labore commodo culpa Lorem. asdqwdadqwdd
                </div>
            </div>   
        
    )
    }
}

export default Card;